# changelog

## 2024-01-03

- added support for custom topic and URL filters, stored locally
- added a toggle to show/hide filtered items
- minor UI improvements
