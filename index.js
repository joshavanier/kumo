const dewidow = (s) => s.replace(/ (?=[^ ]*$)/, "\u00A0");

function parseHash() {
  const { hash } = window.location;
  const base = 100;
  const max = 500;
  const val = !hash ? base : parseInt(hash.substring(1));
  return Number.isNaN(val) ? base : val <= max ? val : max;
}

function Kumo() {
  this.filters = { topics: [], sites: [] };
  this.feed;
  this.count = 20;

  this.loadFilters = () => {
    function _load(filter) {
      const cache = localStorage.getItem(filter);
      return cache ? cache.trim().split("\n") : [];
    }

    this.filters.topics = _load("TOPIC_FILTERS");
    this.filters.sites = _load("URL_FILTERS");
  };

  this.load = (count) => {
    const API = "https://hacker-news.firebaseio.com/v0";
    const { topics, sites } = this.filters;
    const feed = this.feed;

    function shit(str, filter) {
      const query = str.toLowerCase();
      let i = filter.length;
      while (i--) {
        if (query.includes(filter[i])) return true;
      }
      return false;
    }

    function add({ title, url }) {
      if (!url) return;
      feed.append(Object.assign(document.createElement("a"), {
        innerHTML: dewidow(title),
        target: "_blank",
        href: url,
        className: shit(title, topics) || shit(url, sites) ? "hidden" : "",
      }));
    }

    fetch(`${API}/topstories.json`)
      .then((response) => response.json())
      .then((stories) => {
        let i = 0;
        for (const story of stories) {
          if (i >= count) break;
          if (sessionStorage[story]) {
            add(JSON.parse(sessionStorage.getItem(story)));
          } else {
            fetch(`${API}/item/${story}.json`)
              .then((data) => data.json())
              .then(({ title, url }) => {
                sessionStorage.setItem(story, JSON.stringify({ title, url }));
                add({ title, url });
              });
          }
          i++;
        }
      })
      .catch((error) => {
        feed.innerHTML = error;
      });
  };

  this.buildDialog = () => {
    const dialog = document.createElement("dialog");
    const section = document.createElement("section");
    const container = document.createElement("div");

    const topicList = Object.assign(document.createElement("textarea"), {
      value: this.filters.topics.join("\n") || "elon musk",
      onchange: (e) => {
        this.filters.topics = e.target.value.split("\n").filter((x) =>
          x.trim() !== ""
        );
        localStorage.setItem("TOPIC_FILTERS", e.target.value);
      },
    });

    const urlList = Object.assign(document.createElement("textarea"), {
      value: this.filters.sites.join("\n") || "medium.com",
      onchange: (e) => {
        this.filters.sites = e.target.value.split("\n").filter((x) =>
          x.trim() !== ""
        );
        localStorage.setItem("URL_FILTERS", e.target.value);
      },
    });

    const close = Object.assign(document.createElement("button"), {
      innerHTML: "save",
      onclick: () => {
        this.dialog.close();
        this.refresh();
      },
    });

    const instructions = Object.assign(document.createElement("p"), {
      innerHTML: "add keywords and domains to filter out of the feed:",
    });

    section.append(topicList, urlList);
    container.append(instructions, section, close);
    dialog.append(container);
    this.dialog = dialog;
    return dialog;
  };

  this.buildMenu = () => {
    const fragment = new DocumentFragment();

    const button = Object.assign(document.createElement("button"), {
      innerHTML: "filters",
      onclick: () => this.dialog.showModal(),
    });

    const toggle = Object.assign(document.createElement("button"), {
      innerHTML: "show filtered",
      onclick: ({ target }) => {
        target.innerHTML =
          (target.innerHTML === "show filtered" ? "hide" : "show") +
          " filtered";
        for (const el of document.querySelectorAll(".hidden")) {
          el.style.display =
            el.style.display === "none" || el.style.display === ""
              ? "block"
              : "none";
        }
      },
    });

    fragment.append(button, toggle);
    return fragment;
  };

  this.render = () => {
    const fragment = new DocumentFragment();
    const header = document.createElement("header");
    const menu = this.buildMenu();
    const dialog = this.buildDialog();
    const feed = document.createElement("main");

    this.feed = feed;

    header.append(menu, dialog);
    fragment.append(header, feed);
    document.body.append(fragment);
  };

  this.refresh = () => {
    const count = parseHash();
    this.feed.innerHTML = "";
    this.load(count);
  };

  this.init = () => {
    this.loadFilters();
    this.render();
    this.refresh();
    window.onhashchange = () => this.refresh();
  };
}

new Kumo().init();
